### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ b5e1c7a0-afe0-11eb-305e-d9766837bbba
using Pkg

# ╔═╡ c12eb190-afe0-11eb-170d-4ba49d740d41
Pkg.activate("Project.toml")

# ╔═╡ 31ac4b20-b1a4-11eb-340b-b58b2ac2010c


# ╔═╡ c12bcb62-afe0-11eb-05d5-3b63dae23f78
mutable struct CSPVar
	name::String
	
	value::Union{Nothing,Any}
	forbidden_values::Vector{Any}
	domain_restriction_count::Int64
end


# ╔═╡ c12ba450-afe0-11eb-1568-b1c8baa29c04
struct nameCSP
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end


# ╔═╡ 0b5d7ea0-b41d-11eb-13f4-5b1156975f21
md"# Forward Checking  Propagation"

# ╔═╡ f402a040-afe0-11eb-367b-cf9b4475be29
function solve_csp(pb::nameCSP, all_assignments)
		for cur_var in pb.vars
			if cur_var.domain_restriction_count == 4
				return []
			else
				next_val = rand(setdiff(Set([1,2,3,4]),
Set(cur_var.forbidden_values)))

				
		
			  
			
				for cur_constraint in pb.constraints
					if !((cur_constraint[1] == cur_var) || (cur_constraint[2] ==
							cur_var))
						continue
					  else
					if cur_constraint[1] == cur_var
								push!(cur_constraint[2].forbidden_values, next_val)
								cur_constraint[2].domain_restriction_count += 1
						
                    else
                      push!(cur_constraint[1].forbidden_values, next_val)
                       cur_constraint[1].domain_restriction_count += 1
                     

		          end
              end
          end
     
      push!(all_assignments, cur_var.name => next_val)
      end
   end
return all_assignments
end

# ╔═╡ 12a18610-afe1-11eb-1e82-ed9564cb476b
x1 = CSPVar("X1",nothing, [], 0)


# ╔═╡ 1b81b5c0-afe1-11eb-0e03-5bcb442e9045
x2 = CSPVar("X2", nothing, [], 0)

# ╔═╡ 22bf800e-afe1-11eb-21c2-df33d6264e57
x3 = CSPVar("X3", "1", ["2","3","4"], 0)


# ╔═╡ 29bce5fe-afe1-11eb-383e-45f013e4db39
x4 = CSPVar("X4", nothing, [], 0)

# ╔═╡ 2fcd2cd0-afe1-11eb-3965-9dc868d35014
x5 = CSPVar("X5", nothing, [], 0)

# ╔═╡ 45c43100-afe1-11eb-3dd3-5977d722d286
x6 = CSPVar("X6", nothing, [], 0)

# ╔═╡ 49021df0-afe1-11eb-3ac7-3f3f693f26b7
x7 = CSPVar("X7", nothing, [], 0)


# ╔═╡ 52f9d820-afe1-11eb-2c8d-0d7d241b299e
problem = nameCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])



# ╔═╡ 6811cb60-b41d-11eb-143e-3baf571e971e
md"# Running Forward Checking Propagation"

# ╔═╡ 61484240-afe1-11eb-11ef-637b53d25a03
solve_csp(problem, [])

# ╔═╡ Cell order:
# ╠═b5e1c7a0-afe0-11eb-305e-d9766837bbba
# ╠═c12eb190-afe0-11eb-170d-4ba49d740d41
# ╠═31ac4b20-b1a4-11eb-340b-b58b2ac2010c
# ╠═c12bcb62-afe0-11eb-05d5-3b63dae23f78
# ╠═c12ba450-afe0-11eb-1568-b1c8baa29c04
# ╟─0b5d7ea0-b41d-11eb-13f4-5b1156975f21
# ╠═f402a040-afe0-11eb-367b-cf9b4475be29
# ╠═12a18610-afe1-11eb-1e82-ed9564cb476b
# ╠═1b81b5c0-afe1-11eb-0e03-5bcb442e9045
# ╠═22bf800e-afe1-11eb-21c2-df33d6264e57
# ╠═29bce5fe-afe1-11eb-383e-45f013e4db39
# ╠═2fcd2cd0-afe1-11eb-3965-9dc868d35014
# ╠═45c43100-afe1-11eb-3dd3-5977d722d286
# ╠═49021df0-afe1-11eb-3ac7-3f3f693f26b7
# ╠═52f9d820-afe1-11eb-2c8d-0d7d241b299e
# ╠═6811cb60-b41d-11eb-143e-3baf571e971e
# ╠═61484240-afe1-11eb-11ef-637b53d25a03
